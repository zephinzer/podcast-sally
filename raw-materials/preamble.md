# Overview

- central concept of the ledger
- properties we wanted to see
  - prod/dev parity
  - automagic scaling
  - automagic HA (!! very critical)
  - outsourced ops as much as possible
  - one repo, multiple branch multiple deployments, multiple config
  - disposable environments

- architecture diagram

# Stack

- typescript
- serverless framework
- aws
  - apigateway
  - lambda
  - dynamodb
  - waf
  - cloudwatch
  - guard duty
  - xray
  - parameter store
  - sns
  - sqs
- github
- github actions
- seed.run

# Architectural Decisions

- serverless framework for iaac
- serverless infra which provided all the properties we wanted (above)
- runtime config stored in ssm and env
- frontend is configured using a qr code, telling it the url of the endpoint to use and also for user provisioning
- ^ which means that we can point the frontend to any PR or staging or prod environment very easily



# DevOps cool stuff

- every PR gets a full deploy that has parity with prod except for url
- each deploy takes about 5 minutes from commit to healthy
- xray tracing and metrics
- cloudwatch dashboard for monitoring