# Topics & Flow

- [Topics & Flow](#topics--flow)
- [Questions](#questions)
  - [Problem/Solution Domain](#problemsolution-domain)
  - [Architecture and Stack Decisions](#architecture-and-stack-decisions)
  - [Operations and Maintenance](#operations-and-maintenance)
  - [From Development to Production](#from-development-to-production)
  - [Conclusion](#conclusion)

# Questions

## Problem/Solution Domain

- SupplyAlly seems to be more of a backend tool so I'm guessing many might not know how it has played a part in distribution of government-approved resources. For the uninitiated, who are your users and what would a successful end-to-end transaction of SupplyAlly look like for end-users?
- How did the idea for SupplyAlly come about and what drew both of you to join the product team?
- I also see both of you have been working together on OpenCerts in the Distributed Ledger Technologies (DLT) team, how did this experience contribute to the way SupplyAlly was implemented?

## Architecture and Stack Decisions

- If it's not confidential, could you describe what the stack powering SupplyAlly is like?
- I understand that SupplyAlly was created and launched within a short period of time, how would you say that affected the choice of technology stack?

## Operations and Maintenance

- In order for the masks distribution to happen in a sensible manner for citizens, this system obviously needed to scale to go live. What were some of the operations-related concerns the team had and how were they addressed?
- SupplyAlly has been in operation for a few months now, and I'm sure the team has seen it's share of issues. What happens when a bug appears in production?

## From Development to Production

- I believe having a good development environment in terms of a tight feedback cycle and production parity is the keys to delivering awesome software products and I know at least one of you believe so too. What's some magic that you/your team have done to make the developer experience awesome?
- What's the process like from a developer writing code in their IDE to that code being used in production?

## Conclusion

- If you could re-do SupplyAlly after doing it once, what would you change?
