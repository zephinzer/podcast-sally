# Shownotes for SupplyAlly

These are the shownotes for a podcast on the insides of SupplyAlly.

## Overview of SupplyAlly

[SupplyAlly](https://www.supplyally.gov.sg/) is a mobile application created by GovTech Singapore that according to their website: "helps you keep track whether a person receiving an item has received it before". As of June 28th 2020, it has aided in the distribution of 4.2 million masks and 273 laptops. Industry and government users as of June 28th 2020 include: [Engineering Good](https://engineeringgood.org/), [Ministry of Trade and Industry](https://www.mti.gov.sg/), [People's Association](https://www.pa.gov.sg/), and [Community Development Council](https://www.cdc.org.sg/).

This podcast features Rui Jie and Raymond, both developers on the SupplyAlly team, and digs into the technical insides and decisions that make this product which their team had to deliver under somewhat tight constraints.

## Topics

1. Problem/solution domain
2. Architecture and stack decisions
3. Operations and maintenance
4. From development to production

## The People

Rui Jie & Raymond are software engineers in GovTech, in the Distributed Ledger Technology (DLT) team. They are the pioneers working on the OpenCerts project, a framework used for digital notarisation of educational credentials on the Ethereum network. The project has hence spawned a generic document notarisation framework, OpenAttestation. These days, they are mainly occupied with the TradeTrust project, which involves establishing digital standards for digitizing international trade documents

1. Rui Jie: Developer on the SupplyAlly team, Software Engineer at GovTech 
    - Rui Jie is a Singaporean software engineer who was educated in Melbourne and returned when he realised there exists like-minded software craftsmen at GovTech. In his free time he is walked around the neighbourhood by his dog, or otherwise still trying to figure out if the 'god shot' of espresso exists.
2. Raymond Yeh: Developer on the SupplyAlly team, Software Engineer at GovTech
    - Raymond got involved in DLT in 2017 following the cryptocurrency craze. He is recently invested in concepts and literatures of RadicalXChange, decentralized finance and zero-knowledge proofs. His weakness is design. In his free time, he writes and publish ugly applications and simulations for wide range of topics on his blog at geek.sg. 
3. Joseph Matthias Goh: Ops on the MyCareersFuture team - DevOps Engineer at GovTech

## Links

### Media Coverage

- [No short supply of good ideas](https://www.tech.gov.sg/media/technews/no-short-supply-of-good-ideas)
- [A Mask For Each S'porean: How GovTech Built An App In 4 Days To Fairly Distribute 4.2M Masks](https://vulcanpost.com/699204/govtech-supplyally-app-distribute-masks/)
- [Singapore GovTech team helps to develop a mobile app in four days](https://ciotechasia.com/singapore-govtech-team-helps-to-develop-a-mobile-app-in-four-days/)

### Downloads

- [SupplyAlly on Google PlayStore](https://play.google.com/store/apps/details?id=sg.gov.tech.musket&hl=en_SG)
- [SupplyAlly on Apple App Store](https://apps.apple.com/sg/app/supplyally/id1497126533)

### Show References

- [Man arrested, suspected of redeeming more than 200 face masks from vending machines](https://www.channelnewsasia.com/news/singapore/covid-19-man-arrested-200-face-masks-vending-machines-12784984)
